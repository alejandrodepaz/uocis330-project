#include <iostream>
#include <string>
#include "checkers.h"
#include "chess.h"
#define BLACK   "\x1B[30m"
#define RED   "\x1B[31m"
#define WHITE   "\x1B[37m"
#define RESET "\x1B[0m"
#define GREEN  "\x1B[32m"
#define YELLOW  "\x1B[33m"

void printMenu(){
    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;
    std::string green = GREEN;
    std::string yellow = YELLOW;

    //generate top line of border that alternates green and yellow
    for(int i=0; i<40; i++){
        if ((i%2) == 0){
            std::cout << green+"*"+reset;
        } else{
            std::cout << yellow+"*"+reset;
        }
        if(i == 39){
            std::cout << "\n";
        }
    }
    //generate sides of border, while adding text to inside of box
    for(int j=0; j<5; j++){
        if((j%2) == 0){
            std::cout << yellow+"*"+reset << "                                      " << green+"*"+reset << "\n";

        } else{
            if(j==1){
                std::cout << green+"*"+reset << "               GAME BOX               " << yellow+"*"+reset << "\n";
            }else if(j==3){
                std::cout << green+"*"+reset << "           " << yellow+"CHESS"+reset << "    "  << green+"CHECKERS"+reset <<  "          " << yellow+"*"+reset << "\n";
            }else{
                std::cout << green+"*"+reset << "                                      " << yellow+"*"+reset << "\n";
            }
        }
    }
    //generate bottom line of border that alternates green and yellow
    for(int i=0; i<40; i++){
        if ((i%2) == 0){
            std::cout << green+"*"+reset;
        } else{
            std::cout << yellow+"*"+reset;
        }
        if(i == 39){
            std::cout << "\n";
            std::cout << "\n";
        }
    }
}

int main(){

    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;

    while(true){

        if (system("CLS")) system("clear");
        printMenu();
        std::string game_choice;
        std::cout << "Please Select the Game of Your Choice (or 'exit' to quit; spelling is case sensitive): ";
        std::getline(std::cin, game_choice, '\n');
        
        while(!(game_choice == "CHESS" || game_choice == "CHECKERS" || game_choice == "exit")){
            std::cout << "That is not a valid option. Try Again.\n";
            std::cout << "Please Select the Game of Your Choice (or 'exit' to quit; spelling is case sensitive): ";
            std::getline(std::cin, game_choice, '\n');
        }

        if(game_choice == "CHECKERS"){
            Player first_player;
            Player second_player;
            Checkers game;
            if (system("CLS")) system("clear");
            game.playerInit(first_player, second_player);
            //Declare player1 and player2 based on player using white tiles.
            if (first_player.color == white+"◉"+reset){
                game.run(first_player, second_player);
            } else{
                game.run(second_player, first_player);
            }
            std::cin.clear();
            std::cin.ignore( 100 , '\n' );
        } else if(game_choice == "CHESS"){
            Chess game1;
            game1.runGame();
            std::cin.clear();
            std::cin.ignore( 100 , '\n' );
        } else{
            break;
        }

    }
    if (system("CLS")) system("clear");
    return 0;
}