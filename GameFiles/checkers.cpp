#include <iostream>
#include <string>
#include <vector>
#include <sstream>  
#include <thread>
#include <chrono>
#include "checkers.h"
#define BLACK   "\x1B[30m"
#define RED   "\x1B[31m"
#define WHITE   "\x1B[37m"
#define RESET "\x1B[0m"

std::vector<std::string> Checkers::split(std::string str, char delimiter) {
  std::vector<std::string> internal;
  std::stringstream ss(str); // Turn the string into a stream.
  std::string tok;
 
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
 
  return internal;
}

void Checkers::playerInit(Player &player1, Player &player2){
    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;

    player1.name.assign("Player 1");
    player2.name.assign("Player 2");

    std::string color_choice;
    std::cout << "Player 1, Please Select a Color ('red' or 'white', spelling is case sensitive): "; //Get the player's color choice
    std::getline(std::cin, color_choice);
    player1.score = 0;
    player2.score = 0;

    while(!(color_choice.compare("red") == 0 || color_choice.compare("white") == 0)){
        std::cin.clear ();    // Restore input stream to working state
        std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
        std::cout << "Invalid Selection.\n";
        std::cout << "Player 1, Please Select a Color ('red' or 'white', spelling is case sensitive) "; //Get the player's color choice
        std::getline(std::cin, color_choice);
    }
    
    //depending on the color the player selected, update the player's tile color, their king char, and their opponents tile color/king char
    if (color_choice.compare("red") == 0){
        player1.color = red+"◉"+reset;
        player1.king = red+"❃"+reset;
        player1.opponent = white+"◉"+reset;
        player1.opponent_king = white+"❃"+reset;

        player2.color = white+"◉"+reset;
        player2.king = white+"❃"+reset;
        player2.opponent = red+"◉"+reset;
        player2.opponent_king = red+"❃"+reset;

    } else if (color_choice.compare("white") == 0){
            player2.color = red+"◉"+reset;
            player2.king = red+"❃"+reset;
            player2.opponent = white+"◉"+reset;
            player2.opponent_king = white+"❃"+reset;

            player1.color = white+"◉"+reset;
            player1.king = white+"❃"+reset;
            player1.opponent = red+"◉"+reset;
            player1.opponent_king = red+"❃"+reset;
    }

}

void Checkers::boardInit(std::string **game_board, int ROWS, int COLS){
    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;

    int l,m;
    //Add initial white tiles to the board.
    for (l=0; l<3; l++){
        for(m=0; m<COLS; m++){
            if (((l+1)%2 != 0) && ((m+1)%2 == 0)){
                game_board[l][m] = white+"◉"+reset;
            } else if(l == 1 && ((m+1)%2 !=0)){
                game_board[l][m] = white+"◉"+reset;
            }
        }
    }
    //Add initial red tiles to the board.
    for (l=5; l<8; l++){
        for(m=0; m<COLS; m++){
            if ((l%2 != 0) && ((m+1)%2 != 0)){
                game_board[l][m] = red+"◉"+reset;
            } else if(l%2==0 && (m+1)%2 ==0){
                game_board[l][m] = red+"◉"+reset;
            }
        }
    }
    
}

void Checkers::printBoard(std::string **game_board, int ROWS, int COLS, Player first_player, Player second_player){
    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;
    int k, l , m;
    if (system("CLS")) system("clear");
    // With each print of the board, include a reminder of each respective player's color.
    std::cout << "\n";
    std::cout << first_player.name <<" Color: " << first_player.color << "\n";
    std::cout << second_player.name <<" Color: " << second_player.color << "\n";
    std::cout << "\n";

    //simply iterate over the matrix "game_board" and print the corresponding entries.
    for (k=0; k<ROWS; k++){
        if (k == ROWS - 1){
            std::cout << " " << k << "\n";
        }else if (k == 0){
   
            std::cout << "   " << k << " ";
 
        }else{
            std::cout << " " << k << " ";
        }
    }
    for (l=0; l<ROWS; l++){
        for (m=0; m < COLS; m++){
            if (m == COLS - 1){
                std::cout << " " << game_board[l][m] << "\n";
            }else if(m == 0){

                std::cout << l << "  " << game_board[l][m] << " ";

            }else{
                std::cout << " " << game_board[l][m] << " ";
            }
        }
    }
    // With each print of the board, include a reminder of each respective player's score.
    std::cout << "\n";
    std::cout << first_player.name <<" Score: " << first_player.score << "\n";
    std::cout << second_player.name <<" Score: " << second_player.score << "\n";
    std::cout << "\n";
    std::cout << "Type 'exit' to Quit." <<"\n";
    std::cout << "\n";
    std::cout << "\n";

    
}

int Checkers::checkValidDirection(std::string **matrix, int ROWS, int COLS, int init_row, int init_col, std::vector< std::vector<int> > &moves, Player &player){
    /* The two seperate case statements for red and white are done because non-king pieces can only move forward, and because of the
    inherent layout of the matrix, forward means different things on each side of the board. These case statements account for this.*/

    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;
    int new_row, new_col;
    int init_rowcpy = init_row;
    int init_colcpy = init_col;
    bool player_king = false;
    std::vector< std::vector<int> > potn_spots; 
    std::vector< std::vector<int> > potn_spots2; 
    std::vector<int> temp1;
    std::vector<int> temp2;

    if (matrix[init_row][init_col] == red+"◉"+reset){ //Red player movement check
        for(int i=0; i<moves.size(); i++){
            new_row = moves[i][0];
            new_col = moves[i][1];
            //Check if new spot is out of bounds
            if (new_row < 0 || new_row >= ROWS || new_col < 0 || new_col >= COLS ){
                return 0;
            }
            //Check if you're trying to go backwards as a regular piece, and if the destination spot an open spot
            if (new_row > init_row || matrix[new_row][new_col] != "."){
                return 0;
            } 
        
            /*  If a player is making a single move, they can either move one unit (forward) along the diagonal to an open space,
                or be capturing an opponents piece. This if statement captures the empty-space case, and ensure it's a valid move. */
            if(moves.size() == 1 && ((init_row-1 == new_row && init_col+1 == new_col) || (init_row-1 == new_row && init_col-1 == new_col))){
                if (new_row == 0){
                    matrix[new_row][new_col] = player.king;
                }else{
                    matrix[new_row][new_col] = player.color;
                }
                matrix[init_row][init_col] = ".";
                return 1;
            }
            if ((init_row-2 == new_row && init_col+2 == new_col) && (matrix[init_row-1][init_col+1] == player.opponent || matrix[init_row-1][init_col+1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row - 1);
                temp2.push_back(init_col + 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();
                
                init_row = new_row;
                init_col = new_col;
                
            } else if ((init_row-2 == new_row && init_col-2 == new_col) && (matrix[init_row-1][init_col-1] == player.opponent || matrix[init_row-1][init_col-1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row - 1);
                temp2.push_back(init_col - 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();

                init_row = new_row;
                init_col = new_col;

            } else{
                return 0;
            }
        }    
    } else if(matrix[init_row][init_col] == white+"◉"+reset){ //White player movement check
        for(int i=0; i<moves.size(); i++){
            new_row = moves[i][0];
            new_col = moves[i][1];
            //Check if new spot is out of bounds
            if (new_row < 0 || new_row >= ROWS || new_col < 0 || new_col >= COLS ){
                return 0;
            }
            //Check if you're trying to go backwards as a regular piece, and if the destination spot an open spot
            if (new_row < init_row || matrix[new_row][new_col] != "."){
                return 0;
            } 
        
            /*  If a player is making a single move, they can either move one unit (forward) along the diagonal to an open space,
                or be capturing an opponents piece. This if statement captures the empty-space case, and ensure it's a valid move. */
            if(moves.size() == 1 && ((init_row+1 == new_row && init_col+1 == new_col) || (init_row+1 == new_row && init_col-1 == new_col))){
                if (new_row == 7){
                    matrix[new_row][new_col] = player.king;
                }else{
                    matrix[new_row][new_col] = player.color;
                }
                matrix[init_row][init_col] = ".";
                return 1;
            }
            if ((init_row+2 == new_row && init_col+2 == new_col) && (matrix[init_row+1][init_col+1] == player.opponent || matrix[init_row+1][init_col+1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row + 1);
                temp2.push_back(init_col + 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();

                init_row = new_row;
                init_col = new_col;
                
            } else if ((init_row+2 == new_row && init_col-2 == new_col) && (matrix[init_row+1][init_col-1] == player.opponent || matrix[init_row+1][init_col-1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row + 1);
                temp2.push_back(init_col - 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();
                init_row = new_row;
                init_col = new_col;

            } else{
                return 0;
            }
        }
    } else if (matrix[init_row][init_col] == player.king){ // King piece movement check
        player_king = true;
        for(int i=0; i<moves.size(); i++){
            new_row = moves[i][0];
            new_col = moves[i][1];
            //Check if new spot is out of bounds or if the destination spot an open spot
            if (new_row < 0 || new_row >= ROWS || new_col < 0 || new_col >= COLS || matrix[new_row][new_col] != "."){
                return 0;
            }
            /*  If a player is making a single move, they can either move one unit (forward) along the diagonal to an open space,
                or be capturing an opponents piece. This if statement captures the empty-space case, and ensures it's a valid move. */
            if(moves.size() == 1 && ((init_row+1 == new_row && init_col+1 == new_col) || (init_row+1 == new_row && init_col-1 == new_col) || (init_row-1 == new_row && init_col+1 == new_col) || (init_row-1 == new_row && init_col-1 == new_col))){
    
                matrix[new_row][new_col] = player.king;
                matrix[init_row][init_col] = ".";
                return 1;
            }
            if ((init_row-2 == new_row && init_col+2 == new_col) && (matrix[init_row-1][init_col+1] == player.opponent || matrix[init_row-1][init_col+1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row - 1);
                temp2.push_back(init_col + 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();
                
                init_row = new_row;
                init_col = new_col;
                
            } else if ((init_row-2 == new_row && init_col-2 == new_col) && (matrix[init_row-1][init_col-1] == player.opponent || matrix[init_row-1][init_col-1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row - 1);
                temp2.push_back(init_col - 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();

                init_row = new_row;
                init_col = new_col;

            }  else if ((init_row+2 == new_row && init_col+2 == new_col) && (matrix[init_row+1][init_col+1] == player.opponent || matrix[init_row+1][init_col+1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row + 1);
                temp2.push_back(init_col + 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();

                init_row = new_row;
                init_col = new_col;
                
            } else if ((init_row+2 == new_row && init_col-2 == new_col) && (matrix[init_row+1][init_col-1] == player.opponent || matrix[init_row+1][init_col-1] == player.opponent_king)){
                temp1.push_back(init_row);
                temp1.push_back(init_col);
                potn_spots.push_back(temp1);
                temp2.push_back(init_row + 1);
                temp2.push_back(init_col - 1);
                potn_spots2.push_back(temp2);
                temp1.clear();
                temp2.clear();
                init_row = new_row;
                init_col = new_col;

            } else{
                return 0;
            }
        }    

    }
    //If all of the user-specified positions were valid, iterate over the board and remove/place the appropriate tiles. 
    for(int j=0; j<moves.size(); j++){
        new_row = moves[j][0];
        new_col = moves[j][1];
        if ((player.color == red+"◉"+reset && new_row == 0) || (player.color == white+"◉"+reset && new_row == 7) || player_king == true){
            matrix[new_row][new_col] = player.king;
        }else{
            matrix[new_row][new_col] = player.color;
        }
        matrix[potn_spots[j][0]][potn_spots[j][1]] = ".";
        matrix[potn_spots2[j][0]][potn_spots2[j][1]] = ".";
        player.score += 1;
    }
    return 1;
}

int Checkers::noValidMoves(std::string **matrix, int ROWS, int COLS, Player player){
    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;
    int new_row, new_row2, new_row3, new_row4;
    int noValid = 1;

    for (int i=0; i<ROWS; i++){
        for (int j=0; j<COLS; j++){
            if (matrix[i][j] == player.color || matrix[i][j] == player.king){ //Red player movement check
                //Check if new spot is out of bounds
                if (matrix[i][j] == red+"◉"+reset){ //if player is red, we only want to check decreasing rows 
                    new_row = i-1;
                    new_row2 = i-2;
                } else if (matrix[i][j] == white+"◉"+reset){ //if player is white we only want to check increasing rows
                    new_row = i+1;
                    new_row2 = i+2;
                } else if(matrix[i][j] == player.king){ //if player is king, we check rows in both directions
                    new_row = i+1;
                    new_row2 = i+2;
                    new_row3 = i-1;
                    new_row4 = i-2;
                }
                int left_col1 = j-1;
                int right_col1 = j+1;
                int left_col2 = j-2;
                int right_col2 = j+2;

                /* If at any legal move is available, set noValid=0 and break, ultimately returning 0 and forcing the game to continue.
                Otherwise, noValid will remain equal to 1, and the function will print a message stating who no longer has valid moves
                as well as return 1, ensuring that the checkGameOver function ends the game and reports the winner. */
                if ( (new_row >= 0) && (new_row < ROWS) && (left_col1 >= 0) && (left_col1 < COLS)){
                    if (matrix[new_row][left_col1] == "."){
                        noValid = 0;
                        break;
                    }
                } 
                if ( (new_row >= 0) && (new_row < ROWS) && (right_col1 > 0) && (right_col1 < COLS)){
                    if (matrix[new_row][right_col1] == "."){
                        noValid = 0;
                        break;
                    }
                } 
                if((new_row >= 0) && (new_row < ROWS) && (new_row2 >= 0) && (new_row2 < ROWS) && (left_col1 >= 0) && (left_col1 < COLS) && (left_col2 >= 0) && (left_col2 < COLS)){
                    if ((matrix[new_row][left_col1] == player.opponent  && matrix[new_row2][left_col2] == ".") || (matrix[new_row][left_col1] == player.opponent_king && matrix[new_row2][left_col2] == ".")){
                        noValid = 0;
                        break;
                    } 
                }
                if((new_row >= 0) && (new_row < ROWS) && (new_row2 >= 0) && (new_row2 < ROWS) && (right_col1 >= 0) && (right_col1 < COLS) && (right_col2 >= 0) && (right_col2 < COLS)){
                    if ((matrix[new_row][right_col1] == player.opponent && matrix[new_row2][right_col2] == ".") || (matrix[new_row][right_col1] == player.opponent_king && matrix[new_row2][right_col2] == ".")){
                        noValid = 0;
                        break;
                    } 
                }
                if(matrix[i][j] == player.king){ // If current piece is a king, check the other two diagonal positions too.
                    if ( (new_row3 >= 0) && (new_row3 < ROWS) && (left_col1 >= 0) && (left_col1 < COLS)){
                        if (matrix[new_row3][left_col1] == "."){
                            noValid = 0;
                            break;
                        }
                    } 
                    if ( (new_row3 >= 0) && (new_row3 < ROWS) && (right_col1 >= 0) && (right_col1 < COLS)){
                        if(matrix[new_row3][right_col1] == "."){
                            noValid = 0;
                            break;
                        }
                    } 
                    if((new_row3 >= 0) && (new_row4 >= 0) && (new_row3 < ROWS) && (new_row4 < ROWS) && (left_col1 >= 0)  && (left_col2 >= 0) && (left_col1 < COLS)  && (left_col2 < COLS)){
                        //Check if you're trying to go backwards as a regular piece, and if the destination spot is an open spot
                        if ((matrix[new_row3][left_col1] == player.opponent  && matrix[new_row4][left_col2] == ".") || (matrix[new_row3][left_col1] == player.opponent_king && matrix[new_row4][left_col2] == ".")){
                            noValid = 0;
                            break;
                        } 
                    }
                    if((new_row3 >= 0) && (new_row4 >= 0) && (new_row3 < ROWS) && (new_row4 < ROWS) && (right_col1 >= 0)  && (right_col2 >= 0) && (right_col1 < COLS)  && (right_col2 < COLS)){
                        if ((matrix[new_row3][right_col1] == player.opponent && matrix[new_row4][right_col2] == ".") || (matrix[new_row3][right_col1] == player.opponent_king && matrix[new_row4][right_col2] == ".")){
                            noValid = 0;
                            break;
                        } 
                    }
                }
            }
        }
    }

    if (noValid == 1){
        std::cout << "No Valid Moves Remaining For " << player.name << "\n";
        std::cout << "\n";
        return 1;
    } else{
        return 0;
    }
}

int Checkers::checkGameOver(std::string **game_board,int ROWS, int COLS, Player player1, Player player2){
    if (noValidMoves(game_board, ROWS, COLS, player1) || noValidMoves(game_board, ROWS, COLS, player2) || player1.score == 12 || player2.score == 12){
        std::cout << "       GAMEOVER       " << "\n";
        std::cout << "\n";
        std::string winner;
        if(player1.score > player2.score){
            winner = player1.name;
        } else{
            winner = player2.name;
        }
        std::cout << " ******************** " << "\n";
        std::cout << "*                    *" << "\n";
        std::cout << "*  Winner: "<< winner <<"  *" << "\n";
        std::cout << "*                    *" << "\n";
        std::cout << " ******************** " << "\n";
        return 1;
    }
    return 0;

}

int Checkers::run(Player &player1, Player &player2){

    int ROWS = 8;
    int COLS = 8;
    int game_over = 0;

    //Allocate Memory to Initialize Board.
    std::string **game_board = nullptr;
    game_board = new std::string * [ROWS];
    for (int i = 0; i < ROWS; i++) {
        game_board[i] = new std::string[COLS];
        for (int j = 0; j < COLS; j++) {
            game_board[i][j] = ".";
        }
    }
    //Initialize board with initial pieces in their proper positions.
    boardInit(game_board, ROWS, COLS);

    //Print initial board layout. 
    printBoard(game_board, ROWS, COLS, player1, player2);

    std::vector< std::vector<int> > p1_moves;    
    std::vector< std::vector<int> > p2_moves;  

    while (!game_over){

        //Player 1 Input Loop
        while(true){
                
            std::string init_pos;
            std::string dest_pos;
            int init_row, init_col, dest_row, dest_col;
            
            std::cout << player1.name << " Please Specify the Attack Piece Position (e.g. ROWCOL): ";
            std::cin >> init_pos;
            if(init_pos == "exit"){
                return 0;
            }
            init_row = init_pos[0] - '0';
            init_col = init_pos[1] - '0';

            while (init_row < 0 || init_row > 7 || init_col < 0 || init_col > 7 || (game_board[init_row][init_col] != player1.color && game_board[init_row][init_col] != player1.king)){
                std::cin.clear ();    // Restore input stream to working state
                std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
                std::cout << "Invalid Move, Try Again.\n";
                std::cout << player1.name << " Please Specify the Attack Piece Position (e.g. ROWCOL): ";
                std::cin >> init_pos;  
                if(init_pos == "exit"){
                    return 0;
                }
                init_row = init_pos[0] - '0';
                init_col = init_pos[1] - '0'; 
            }

            std::cin.ignore ( 100 , '\n' ); 
            std::cout << player1.name << " Please Specify the Destination (e.g. ROWCOL): ";
            std::getline(std::cin, dest_pos);
            if(dest_pos == "exit"){
                return 0;
            }

            std::vector<std::string> temp_vals = split(dest_pos, ' ');
            for(int i=0; i<temp_vals.size(); i++){
                std::vector<int> temp_pos;
                dest_row = temp_vals[i][0] - '0';
                dest_col = temp_vals[i][1] - '0';
                temp_pos.push_back(dest_row);
                temp_pos.push_back(dest_col);
                p1_moves.push_back(temp_pos);
            }
            for(int j=0; j<p1_moves.size(); j++){
                std::cout<<"Dest Row: " << p1_moves[j][0] << " Dest Col: " << p1_moves[j][1] << "\n";
            }
            //If checkValidDirection returns 0, the player is asked to try again.
            if(!(checkValidDirection(game_board, ROWS, COLS, init_row, init_col, p1_moves, player1))){
                p1_moves.clear();
                std::cout << "Invalid Move, Try Again." << "\n";
            } else{ //Otherwise, clear the moves stored in the respective player's move deck, and display the updated board.
                p1_moves.clear();
                
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                printBoard(game_board, ROWS, COLS, player1, player2);
                break;
            }
        }

        if (checkGameOver(game_board, ROWS, COLS, player1, player2)){
            break;
        }

        //Player 2 Input Loop
        while(true){
                
            std::string init_pos;
            std::string dest_pos;
            int init_row, init_col, dest_row, dest_col;
            
            std::cout << player2.name << " Please Specify the Attack Piece Position (e.g. ROWCOL): ";
            std::cin >> init_pos;
            if(init_pos == "exit"){
                return 0;
            }
            init_row = init_pos[0] - '0';
            init_col = init_pos[1] - '0';

            while (init_row < 0 || init_row > 7 || init_col < 0 || init_col > 7 || (game_board[init_row][init_col] != player2.color && game_board[init_row][init_col] != player2.king)){
                std::cin.clear ();    // Restore input stream to working state
                std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
                std::cout << "Invalid Move, Try Again.\n";
                std::cout << player2.name << " Please Specify the Attack Piece Position (e.g. ROWCOL): ";
                std::cin >> init_pos;  
                if(init_pos == "exit"){
                    return 0;
                }
                init_row = init_pos[0] - '0';
                init_col = init_pos[1] - '0'; 
            }
            std::cin.ignore ( 100 , '\n' ); 
            std::cout << player2.name << " Please Specify the Destination (e.g. ROWCOL): ";
            std::getline(std::cin, dest_pos);
            if(dest_pos == "exit"){
                return 0;
            }
            //split the user provided moveset by spaces, assuming more than one move was provided.
            std::vector<std::string> temp_vals = split(dest_pos, ' ');
            for(int i=0; i<temp_vals.size(); i++){
                    std::vector<int> temp_pos;
                    dest_row = temp_vals[i][0] - '0';
                    dest_col = temp_vals[i][1] - '0';
                    temp_pos.push_back(dest_row);
                    temp_pos.push_back(dest_col);
                    p2_moves.push_back(temp_pos);
            }
            for(int j=0; j<p2_moves.size(); j++){
                std::cout<<"Dest Row: " << p2_moves[j][0] << " Dest Col: " << p2_moves[j][1] << "\n";
            }
            //If checkValidDirection returns 0, the player is asked to try again.
            if(!(checkValidDirection(game_board, ROWS, COLS, init_row, init_col, p2_moves, player2))){
                p2_moves.clear();
                std::cout << "Invalid Move, Try Again." << "\n";
            } else{ //Otherwise, clear the moves stored in the respective player's move deck, and display the updated board.
                p2_moves.clear();
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                printBoard(game_board, ROWS, COLS, player1, player2);
                break;
            }
        }
        if (checkGameOver(game_board, ROWS, COLS, player1, player2)){
            break;
        }
    }
    int d;
    for (d = 0; d < ROWS; d++)
		delete [] game_board[d];
	delete [] game_board;

    return 0;

}
/*
int main(){
    std::string red = RED;
    std::string white = WHITE;
    std::string reset = RESET;

    Player first_player;
    Player second_player;
    Checkers game;
    if (system("CLS")) system("clear");
    game.playerInit(first_player, second_player);
    //Declare player1 and player2 based on player using white tiles.
    if (first_player.color == white+"◉"+reset){
        game.run(first_player, second_player);
    } else{
        game.run(second_player, first_player);
    }
    return 0;
}
*/
