Willis:

This first week of development on the project for me has consisted of doing more research into our QT framework.  
One of the first things I did this week was look into the other game creation frameworks that you shared with us on
canvas.  As our goal is to create a gamebox where players can play traditional board gammes, I thought these may be a
good resource to us.  However, upon further investigation it seemed that these libraries are designed primarly around 
and actively listening ever changing environment.  While this would serve well any game from super mario to call of 
duty, it is not the best framework for our more menu based, turn taking project.  Thus it was concluded that QT would
our best bet.

The primary, and most daunting task at the moment is to familiarize ourselves with the QT IDE and processes.  Having 
watched a handful of instructional videos, I am beginning to feel more comfortable with the QT ecosystem.  However, 
Alejandro and I are concerned that teaching ourselves this entire framework may be biting off more than we can chew by
the end of the term,  We are considering not employing a gui at all and instead may try and create a very pleasing 
game representation in the console.  Doing so we would be able to prioritize more of our time of c++ based tasks like
making sure we create our different game classes in robust and thoughtful ways.  I feel like these things involving 
class inheritance and more traditional input/output may be more effective things to learn and further practice this 
term.  Uploaded to bitbucket today is just the initial QT project, originally created on my device, that we are hoping
and anticipating will properly transfer to Alejandro's new IDE as well.  In the next very few days I would like us to 
make a commited decision to use QT or not. 

Alejandro De Paz:

	At the moment, and as mentioned above, Willis and I are currently researching the Qt library and trying to determine
if learning the details of this framework is going to be an efficient allocation fo our time. While creating a basic 
user interface seems simple enough based off of beginner tutorials I have watched, I believe integrating a responsive 
game on our current time budget may result in an incomplete project in the timeframe we currently have. I feel confident 
in writing the logic for a couple board games that could operate in the terminal, and have already begun writing pseudo-code 
for how these might be implemented. This being said, I have found a tutorial which implements a basic platform game, 
which I believe could be useful in helping us develop the functionality needed for a board game. I would like to further 
investigate the existence of Qt examples which implement features similar to those we seek to create, as this would greatly 
aid us in creating a basic GUI while saving us precious time. 
	During this next week, I intend to complete this game creation series on YouTube, while simultaneously creating the 
backend checkers game logic that can at the very least be fed theoretical gameplay data and produce a table of the 
corresponding (accurate) results. If all goes well, we will settle on either following through with the Qt framework, 
or simply abandon it and begin a terminal implementation of our GameBox idea.