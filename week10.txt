Alejandro:

During week 10, I focused primarly on playing multiple runs of my game in 
order to ensure that all edge cases had been properly accounted for -- or even
recognized in the first place. In doing so I was able to fix a couple obscure
bugs, as well as clarify input-request statements. I also worked on our group
presentation, researching the history of Checkers (which is actually, to my
surprise, very interesting), as well as formalizing a set of rules to describe
how the game is played. Finally, I was able to complete the Main Menu feature
for displaying our games at startup. With regards to my contributions, I 
worked primarily on Checkers and the Main Menu, designing and scripting the 
layout and logic for both. This being said, both Willis and I routinely 
communicated in person to discuss the logistics of how we wanted to ultimately
combine the games, as well as provided help/shared tips and tricks throughout
the process. Early on in the process, we both struggled with determining an 
effective way to read store intermediary data in an efficient way.
After discussing different possibilities, we agreed that vectors would be
the most effective means. We also spents numerous hours writing pseudo-code
exploring different ways to implement game-logic, sharing our results and 
essentially poking holes in any faulty logic that may exist. 
    
Overall, this experience has been extremely educational, giving me exposure to
vectors, new string operations (delimiters,UTF-8 encoding, ANSI escape codes),
and team work. I also thoroughly enjoyed the creative process it afforded when
determining how game logic should be implemented, and I was ultimately able
to write 3 different versions of how the validity of move for any piece in any 
position could be checked, revising them and ultimately combining the effective
aspects of each into one robust version. Over the next few months, Willis and 
I seek to attempt implementing our games in SFML, as a more user-friendly 
interface is definitely desired. In retrospect, I would have liked to have 
settled on a console-based GameBox sooner, as the amount of energy and time
resources we invested in trying to learn Qt took away a substantial amount 
potential effort that could've been further invested in more games for our 
GameBox; thus, in the future I want to focus on better managing my time to 
account for extrenuous factors that might affect long-term performance.     
