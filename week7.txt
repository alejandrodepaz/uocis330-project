After finishing the caesar cipher this week, we were unable to accomplish as much as we would have liked to
but we made progress none the less.  Firstly, we've decided we do not want to implement the Qt framework.  
We've made this decision because of many different reasons.  Those reasons include that my PC was having a
hardtime setting up the qt creator ide.  I tried many different downloads and none of them were able to even
run the sample programs.  This is obviously an error on my end but this process has been time consuming. 
NExt, if we chose to do Qt, it is almost certain that we would only be able to implement chess as maximum
functionality.  If we run it out of the terminal, we can definitely have more games.  Currently our goals 
is to implement chess and checkers.  

However, there was always an issue with running the games out of the terminal.  Those were obviously the 
way that users would interact with the game, and how to display it.  Well when thinking about the Qt 
framework, we had concluded the best way to take in user information would be to make a matrix of buttons.  
We'd click the location of a peice, and then where it would go.  We would have liked to do drag n drop as 
well but that would have defintely been to extensive.  However in the terminal we can only enter information
through the keyboard.  THus people will have to enter the coordinates of a starting piece and the location
for it to go to.  This is not ideal but this will have to be.  Hopefully we can make the "print" functions
all very aesthetically pleasing to compensate for this. 

That second issue mentioned above was how to represent and display the boards.  Just today I learned that you
can in fact print in color in the terminal.  Having excecuted this on my device and ix-dev I am satsified with
the functionality.  We can represent each team with different colors and therefore we don't have to learn
much new features, we can focus on functionality.  I am excited because I feel like I can now truely make
leaps with this project.  Again, I'd wished we'd done more this week but I am happy with where we are going
from here.

Alejandro: 

As has been discussed above, we have decided to change direction with this project in hopes that we might 
make a more comprehensive project that -- while prehaps not as 'user friendly' -- will serve more features and 
and a introduce a greater breadth of components than would have been possible to implement with our prior 
strategy. This being the case, both Willis and I have already made much more constructive headway than in the previous week,
when we had to allocate all of our time in order to teach ourselves the Qt framework rather than actually begin
the implementation process. We have already discussed the approach we would like to take with this project, by
effectively splitting the two games -- chess and checkers -- into two seperate workloads, where each of us can
focus on one of the games. While the games will be run from the terminal (or command line), we hope to utilize 
color-printing features and any other possible feature accents to enhance the user experience to its greatest
potential. Moving forward, I hope to begin implementing the specific game logic, as the functions/classes have
been succesfully outlined in a header file and implementation of printing functions are nearly complete as of 
this upload timestamp. I also want to finalize how users will be prompted for making a move in each game, as this
affects the UX greatly. As of right now, the user must specify a [ROW, COLUMN] tupple, but I think there may be a
more elegant solution. 
 
