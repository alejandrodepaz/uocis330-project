#include <iostream>
#include <unordered_map>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>
#include <iterator>
#include <unistd.h>
using namespace std;
#include "chess.h"


void Chess::setBoard(){
  //This function is called first in runGame method.  It sets the layout of the board
 //Can be actively modified to check different game states with ease
  board = new char * [8];
  for (int i = 0; i < 8; i++){
    board[i] = new char[8];
    for (int j = 0; j < 8; j++)
      board[i][j] = '.';
  }
  board[0][0] = 'T';
  board[0][1] = 'H';
  board[0][2] = 'B';
  board[0][3] = 'Q';
  board[0][4] = 'J';
  board[0][5] = 'B';
  board[0][6] = 'H';
  board[0][7] = 'T';


  board[1][0] = 'O';
  board[1][1] = 'O';
  board[1][2] = 'O';
  board[1][3] = 'O';
  board[1][4] = 'O';
  board[1][5] = 'O';
  board[1][6] = 'O';
  board[1][7] = 'O';

  board[6][0] = 'o';
  board[6][1] = 'o';
  board[6][2] = 'o';
  board[6][3] = 'o';
  board[6][4] = 'o';
  board[6][5] = 'o';
  board[6][6] = 'o';
  board[6][7] = 'o';

  board[7][0] = 't';
  board[7][1] = 'h';
  board[7][2] = 'b';
  board[7][3] = 'q';
  board[7][4] = 'j';
  board[7][5] = 'b';
  board[7][6] = 'h';
  board[7][7] = 't';
}

void Chess::printBoard(char player){
  //This unordered_map allows for us to create our 2D array representing the board
  //which different values but when we display the information through the map
  //it will appear in an interpretable fashion
  unordered_map<char, string> convert = {
    {'P',  "P"},
    {'R',  "R"},
    {'H',  "H"},
    {'B',  "B"},
    {'K',  "K"},
    {'Q',  "Q"},

    {'O',  "P"},
    {'T',  "R"},
    {'J',  "K"},

    {'p',  "\033[1;31mP\033[0m"},
    {'r',  "\033[1;31mR\033[0m"},
    {'h',  "\033[1;31mH\033[0m"},
    {'b',  "\033[1;31mB\033[0m"},
    {'k',  "\033[1;31mK\033[0m"},
    {'q',  "\033[1;31mQ\033[0m"},

    {'o',  "\033[1;31mP\033[0m"},
    {'t',  "\033[1;31mR\033[0m"},
    {'j',  "\033[1;31mK\033[0m"},


    {'.', "."}
  };

  //Clear the users current terminal window
  system("clear");

  //If accurate character is given to function, print out respective players name
  if (player == 'w'){
    cout << "\n It is the White Players turn\n";
  }
  else if (player == 'r'){
    cout << "\n It is the \033[1;31mRed\033[0m Players turn\n";
  }

  //First for loop prints out the top row of the board
    for (int i = 1; i <= 8; i++){

      if (i == 1){
        cout << "\n  " << i;
      }
      else if(i == 8){
        if (i < 10)
          cout << " " << i << endl;
        else
          cout << i << endl;
      }
      else{
        if (i < 10)
          cout << " " << i;
        else
          cout << i;
      }
    }

    //Second loop prints the contents of the board, the left column, and the key
    for (int i = 0; i < 8; i++){

      if (i < 9)
        cout << i + 1 << " ";
      else
        cout << i + 1;
      for (int j = 0; j < 8; j++){
        if (i+1 == 8){
          if (j+1 == 8)
            cout << convert[board[i][j]] << "\n";
          else
            cout << convert[board[i][j]] << " ";
        }
        else {
          if (j+1 == 8)
            cout << convert[board[i][j]];
          else
            cout << convert[board[i][j]] << " ";
        }
      }

      if (i+1 == 2)
        cout << "      'K' = King" << endl;
      else if (i+1 == 3)
        cout << "      'Q' = Queen" << endl;
      else if (i+1 == 4)
        cout << "      'B' = Bishop" << endl;
      else if (i+1 == 5)
        cout << "      'H' = Knight/Horse" << endl;
      else if (i+1 == 6)
        cout << "      'R' = Rook/Castle" << endl;
      else if (i+1 == 7)
        cout << "      'P' = Pawn" << endl;
      else
        cout << endl;
    }
  }

int * Chess::getPos(char player){
  //The folowing method obtains the row and column of the piece they want to move
  //Allows for user to castle, if so, user enters 101 for initial row and 101 is
  //also returned by the method

  //Set up current players array of characters
  char redPlayer[9] = {'p','r','h','b','q','k','o','t','j'};
  char whitePlayer[9] = {'P', 'R', 'H', 'B', 'Q', 'K','O','T','J'};
  char curPlayer[9];
  static int coords[2];

  int check = 0;
  if (player == 'w'){
    memcpy(curPlayer, whitePlayer, sizeof(curPlayer));
  }
  else if (player == 'r'){
    memcpy(curPlayer, redPlayer, sizeof(curPlayer));
  }

//This loop prompts users to enter information on row and column or castle
//Checks that input is acceptable single digit integer and that a proper piece
//is as that position.  printBoard is often called when wrong input is given
//because we don't want a long string of input going down such that the user
//couldn't see the board
  redo:
  do{
    printBoard(player);
    cout << "Which piece do you want to move? (Enter '101' to castle) (Enter '-1' to exit game)\n\nEnter the row, press the 'enter' key, then the column\n\nRow: ";
    cin >> coords[0];
    if (cin.fail()){
      printBoard(player);
      cout << "\nWhat you entered was not an integer, please try again\n\n" << endl;
      cin.clear();
      cin.ignore(100,'\n');
    }
    else if (coords[0] == -1){
      return coords;
    }
    else if (coords[0] == 101){
      static char ch;
      int check2 = 0;
      cin.clear();
      cin.ignore(100, '\n');
      do {
        cout << "\nWould you like to castle? Enter 'L' to castle left, 'R' to castle right, or 'C' to cancel\n\nCastle: ";
        cin >> ch;
        if (cin.fail()){
          cout << "\nWhat you entered was not an option, please try again" << endl;
          cin.clear();
          cin.ignore(100, '\n');
        }
        //The following (ugly) loops are present to allow for castling.  They return row = 101
        else if (ch == 'L'){
          if (player == 'r'){
            if ((board[7][0] == 't') && (board[7][1] == '.') && (board[7][2] == '.') && (board[7][3] == '.') && (board[7][4] == 'j')){
              board[7][0] = '.';
              board[7][2] = 'k';
              board[7][3] = 'r';
              board[7][4] = '.';
              coords[0] = 101;
              return coords;
            }
            else{
              cout << "\nInvalid Castle, please try again\n";
            }
          }
          else{
            if ((board[0][0] == 'T') && (board[0][1] == '.') && (board[0][2] == '.') && (board[0][3] == '.') && (board[0][4] == 'J')){
              board[0][0] = '.';
              board[0][2] = 'K';
              board[0][3] = 'R';
              board[0][4] = '.';
              coords[0] = 101;
              return coords;
            }
            else{
              cout << "\nInvalid Castle, please try again\n";
            }
          }
        }
        else if (ch == 'R'){
          if (player == 'r'){
            if ((board[7][7] == 't') && (board[7][6] == '.') && (board[7][5] == '.') && (board[7][4] == 'j')){
              board[7][7] = '.';
              board[7][6] = 'k';
              board[7][5] = 'r';
              board[7][4] = '.';
              coords[0] = 101;
              return coords;
            }
            else{
              cout << "\nInvalid Castle, please try again\n";
            }
          }
          else{
            if ((board[0][7] == 'T') && (board[0][6] == '.') && (board[0][5] == '.') && (board[0][4] == 'J')){
              board[0][7] = '.';
              board[0][6] = 'K';
              board[0][5] = 'R';
              board[0][4] = '.';
              coords[0] = 101;
              return coords;
            }
            else {
              cout << "\nInvalid Castle, please try again\n";
            }
          }
        }
        else if (ch == 'C'){
          cin.clear();
          cin.ignore(100, '\n');
          goto redo;
        }
        else {
          cout << "\nWhat you entered was not an option, please try again" << endl;
          cin.clear();
          cin.ignore(100, '\n');
      }
    } while(check2 == 0);
  }
    else if (coords[0] < 1 || 8 < coords[0]){
      printBoard(player);
      cout << "\nWhat you entered was not in the accepted range, please try again\n\n" << endl;
    }
    else{
      cout << "Col: ";
      cin >> coords[1];
      if (cin.fail()){
        printBoard(player);
        cout << "\nWhat you entered was not an integer, please try again\n" << endl;
        cin.clear();
        cin.ignore(100,'\n');
      }
      else if (coords[1] < 1 || 8 < coords[1]){
        printBoard(player);
        cout << "\nWhat you entered was not in the accepted range, please try again\n" << endl;
      }
      else{
        char a = board[coords[0]-1][coords[1]-1];
        if (find(begin(curPlayer), end(curPlayer), a) != end(curPlayer))
          check = 1;
        else {
          printBoard(player);
          cout << "\nThe position you choose does not contain a piece of yours, please try again\n" << endl;
        }
      }
    }
  } while(check == 0);
  coords[0] -= 1;
  coords[1] -= 1;
  return coords;
}

int** Chess::getValids(int row, int col, char player){
  //in this function we will take a users row and column and check the piece at that location
  //we will check that piece and location for all its possible moves and return all possibilities
  //in a 2-dimensional array

  char piece = board[row][col];

  //Prepare our return array
  int** moves = 0;
  moves = new int*[21];
  for (int i = 0; i < 21; i++){
    moves[i] = new int[2];
  }
  int cntr = 0;

  //Depending on the current player, set which array of characters is the opponent
  char redPlayer[9] = {'p','r','h','b','q','k', 'o','t','j'};
  char whitePlayer[9] = {'P', 'R', 'H', 'B', 'Q', 'K','O','T','J'};
  char opponent[9];
  if (player == 'w'){
    memcpy(opponent, redPlayer, sizeof(opponent));
  }
  else if (player == 'r'){
    memcpy(opponent, whitePlayer, sizeof(opponent));
  }


  if ((piece == 'P')|| (piece == 'O')){
    //if piece is white pawn
    if (piece == 'O'){
      //if piece hasn't moved yet
      if ((row+2 >= 0) && (row+2 <= 7) && (col >= 0) && (col <= 7)){
        //if space directly in front is valid
        if (board[row +2][col] == '.'){
          moves[cntr][0] = row + 2;
          moves[cntr][1] = col;
          cntr += 1;
        }
      }
    }
    if ((row+1 >= 0) && (row+1 <= 7) && (col >= 0) && (col <= 7)){
      //if space directly in front is valid
      if (board[row +1][col] == '.'){
        moves[cntr][0] = row + 1;
        moves[cntr][1] = col;
        cntr += 1;
      }
    }
    if ((row+1 >= 0) && (row+1 <= 7) && (col-1 >= 0) && (col-1 <= 7)){

      char temp = board[row+1][col-1];
      if (find(begin(opponent), end(opponent), temp) != end(opponent)){
        moves[cntr][0] = row + 1;
        moves[cntr][1] = col - 1;
        cntr += 1;
      }
    }
    if ((row+1 >= 0) && (row+1 <= 7) && (col+1 >= 0) && (col+1 <= 7)){
      char temp2 = board[row+1][col+1];
      if (find(begin(opponent), end(opponent), temp2) != end(opponent)){
        moves[cntr][0] = row + 1;
        moves[cntr][1] = col + 1;
        cntr += 1;
      }
    }
  }

  else if ((piece == 'p') || (piece == 'o')){
    //if piece is black pawn
    if (piece == 'o'){
      //if piece hasn't moved yet
      if ((row-2 >= 0) && (row-2 <= 7) && (col >= 0) && (col <= 7)){
        //if space directly in front is valid
        if (board[row -2][col] == '.'){
          moves[cntr][0] = row - 2;
          moves[cntr][1] = col;
          cntr += 1;
        }
      }
    }
    if ((row-1 >= 0) && (row-1 <= 7) && (col >= 0) && (col <= 7)){
      //if space directly in front is valid
      if (board[row -1][col] == '.'){
        moves[cntr][0] = row - 1;
        moves[cntr][1] = col;
        cntr += 1;
      }
    }
    if ((row-1 >= 0) && (row-1 <= 7) && (col-1 >= 0) && (col-1 <= 7)){

      char temp = board[row-1][col-1];
      if (find(begin(opponent), end(opponent), temp) != end(opponent)){
        moves[cntr][0] = row - 1;
        moves[cntr][1] = col - 1;
        cntr += 1;
      }
    }
    if ((row-1 >= 0) && (row-1 <= 7) && (col+1 >= 0) && (col+1 <= 7)){
      char temp2 = board[row-1][col+1];
      if (find(begin(opponent), end(opponent), temp2) != end(opponent)){
        moves[cntr][0] = row - 1;
        moves[cntr][1] = col + 1;
        cntr += 1;
      }
    }
  }

  else if ((piece == 'H') || (piece == 'h')){
    int directions[8][2] = {{-2,-1},{-2,1},{-1,2},{1,2},{2,-1},{2,1},{-1,-2},{1,-2}};
    int val1, val2;
    char temp;
    for (int i = 0; i<8; i++){
      val1 = row+directions[i][0];
      val2 = col+directions[i][1];
      if ((val1 >= 0) && (val1 <= 7) && (val2 >= 0) && (val2 <= 7)){
        temp = board[val1][val2];
        if ((find(begin(opponent), end(opponent), temp) != end(opponent)) || (temp == '.')){
          moves[cntr][0] = val1;
          moves[cntr][1] = val2;
          cntr += 1;
        }
      }
    }
  }

  else if ((piece =='R') || (piece == 'r') || (piece == 'T') || (piece == 't')){
    //The following if/else statements help create the "castle" possibility
    /*if ((piece == 'T') && (row == 0) && (col == 0) && (board[row][1] == '.') && (board[row][2] == '.') && (board[row][3] == '.') && (board[0][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    else if ((piece == 'T') && (row == 0) && (col == 7) && (board[row][6] == '.') && (board[row][5] == '.') && (board[0][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    else if ((piece == 't') && (row == 7) && (col == 0) && (board[row][1] == '.') && (board[row][2] == '.') && (board[row][3] == '.') && (board[row][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    else if ((piece == 't') && (row == 7) && (col == 7) && (board[row][6] == '.') && (board[row][5] == '.')  && (board[row][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }*/
    int directions[4][2] = {{-1,0},{1,0},{0,-1},{0,1}};
    int tempVal1, tempVal2;
    char temp;
    for (int i=0; i<4; i++){
      tempVal1 = row;
      tempVal2 = col;
      while(1){
        tempVal1 +=directions[i][0];
        tempVal2 +=directions[i][1];
        if ((tempVal1 >= 0) && (tempVal1 <= 7) && (tempVal2 >= 0) && (tempVal2 <= 7)){
          temp = board[tempVal1][tempVal2];
          if (temp == '.'){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
          }
          else if ((find(begin(opponent), end(opponent), temp) != end(opponent))){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
            break;
          }
          else {
            break;
          }
        }
        else{
          break;
        }
      }
    }
  }

  else if ((piece == 'B') || (piece == 'b')){
    int directions[4][2] = {{-1,-1},{-1,1},{1,1},{1,-1}};
    int tempVal1, tempVal2;
    char temp;
    for (int i=0;i<4;i++){
      tempVal1=row;
      tempVal2=col;
      while(1){
        tempVal1 +=directions[i][0];
        tempVal2 +=directions[i][1];
        if ((tempVal1 >= 0) && (tempVal1 <= 7) && (tempVal2 >= 0) && (tempVal2 <= 7)){
          temp = board[tempVal1][tempVal2];
          if (temp == '.'){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
          }
          else if ((find(begin(opponent), end(opponent), temp) != end(opponent))){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
            break;
          }
          else {
            break;
          }
        }
        else{
          break;
        }
      }
    }
  }

  else if ((piece == 'Q') || (piece == 'q')){
    int directions[8][2] = {{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1}};
    int tempVal1, tempVal2;
    char temp;
    for (int i=0;i<8;i++){
      tempVal1=row;
      tempVal2=col;
      while(1){
        tempVal1 +=directions[i][0];
        tempVal2 +=directions[i][1];
        if ((tempVal1 >= 0) && (tempVal1 <= 7) && (tempVal2 >= 0) && (tempVal2 <= 7)){
          temp = board[tempVal1][tempVal2];
          if (temp == '.'){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
          }
          else if ((find(begin(opponent), end(opponent), temp) != end(opponent))){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
            break;
          }
          else {
            break;
          }
        }
        else{
          break;
        }
      }
    }
  }

  else if ((piece == 'K') || (piece == 'k') || (piece == 'J') || (piece == 'j')){
    int directions[8][2] = {{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1}};
    int val1, val2;
    char temp;
    for (int i = 0; i<8;i++){
      val1 = row+directions[i][0];
      val2 = col+directions[i][1];
      if ((val1 >= 0) && (val1 <= 7) && (val2 >= 0) && (val2 <= 7)){
        temp = board[val1][val2];
        if ((find(begin(opponent), end(opponent), temp) != end(opponent)) || (temp == '.')){
          moves[cntr][0] = val1;
          moves[cntr][1] = val2;
          cntr += 1;
        }
      }
    }
  }

  /*
  cout << "\n\n\n";
  for (int i = 0; i < 21; i++){
    cout << "row: " << moves[i][0]+1 << "  col:  " << moves[i][1]+1 << '\n';
  }
  cout << "\n\n\n";
  */
  return moves;
}

int * Chess::getMove(char player){
  //The folowing method obtains the row and column of where player wants to move to.
  //POsition gets checked
  //that it is in fact empty or an enemy piece
  char redPlayer[9] = {'p','r','h','b','q','k','o','t','j'};
  char whitePlayer[9] = {'P', 'R', 'H', 'B', 'Q', 'K','O','T','J'};
  char opponent[9];
  static int coords[2];


  int check = 0;
  if (player == 'w'){
    memcpy(opponent, redPlayer, sizeof(opponent));
  }
  else if (player == 'r'){
    memcpy(opponent, whitePlayer, sizeof(opponent));
  }


  do{
    cout << "\nWhere do you want to move to?\n\nEnter the row, press the 'enter' key, then the column (Enter invalid empty position to reselec piece)\n\nRow: ";
    cin >> coords[0];
    if (cin.fail()){
      printBoard(player);
      cout << "\nWhat you entered was not an integer, please try again\n\n" << endl;
      cin.clear();
      cin.ignore(100,'\n');
    }
    else if (coords[0] < 1 || 8 < coords[0]){
      printBoard(player);
      cout << "\nWhat you entered was not in the accepted range, please try again\n\n" << endl;
    }
    else{
      cout << "Col: ";
      cin >> coords[1];
      if (cin.fail()){
        printBoard(player);
        cout << "\nWhat you entered was not an integer, please try again\n" << endl;
        cin.clear();
        cin.ignore(100,'\n');
      }
      else if (coords[1] < 1 || 8 < coords[1]){
        printBoard(player);
        cout << "\nWhat you entered was not in the accepted range, please try again\n" << endl;
      }
      else{
        char a = board[coords[0]-1][coords[1]-1];
        if ((find(begin(opponent), end(opponent), a) != end(opponent)) || (a == '.'))
          check = 1;
        else {
          printBoard(player);
          cout << "\nThat was invalid move.  Try again" << endl;
        }
      }
    }
  } while(check == 0);
  coords[0] -= 1;
  coords[1] -= 1;
  return coords;
}

bool Chess::isValid(int **moves, int *move){
  //This method takes a 2D list of valid possible moves and iterates over it to make sure that the players
  //desired move is valid
  for (int i=0; i<21; i++){
    int row2 = moves[i][0];
    int col2 = moves[i][1];
    if ((row2 >= 0) && (row2 <= 7) && (col2 >= 0) && (col2 <= 7)){
      int row1 = move[0];
      int col1 = move[1];
      if ((row1 == row2) && (col1 == col2)){
        return true;
      }
    }
  }
  return false;
}

void Chess::makeMove(int row1, int col1, int row2, int col2, char player){
  //This method takes the coordinates of a piece and the location it should move to and the player moving.
  //At this point we have checked taht there is a piece at that position and the position if wants to go to
  //is a valid result location.
  //This method will move the piece and put a piece into a graveyard if necessary
  vector<char> curGrave;

  char piece = board[row1][col1];
  board[row1][col1] = '.';
  if (board[row2][col2] != '.'){
    //update graveyards with fallen pieces
    if (player == 'w'){
      redGrave.push_back(board[row2][col2]);
    }
    if (player == 'r'){
      whiteGrave.push_back(board[row2][col2]);
    }
  }
  //The following conversions change pawns, kings, and rooks from a state of never
  //being moved to having been moved at least once
  if (piece == 'o')
    piece = 'p';
  else if (piece == 'O')
    piece = 'P';
  else if (piece == 't')
    piece = 'r';
  else if (piece == 'T')
    piece = 'R';
  else if (piece == 'j')
    piece = 'k';
  else if (piece == 'J')
    piece = 'K';

  //These 2 if statements help us catch if a pawn has made it to the far side of the
  //board and can therefore be replaced with any fallen piece

  else if ((piece == 'p') && (row2 == 0)){
    int len = redGrave.size();
    char ch;
    int check3 = 0;
    do {
      printBoard(player);
      cout << "Please choose a fallen piece to replace your pawn with:\n\n  ";
      for (int i = 0; i<len; i++){
        if (redGrave[i] != 'p')
          cout << "'" << redGrave[i] << "'  ";
      }
      cout <<"\n\nWhich Piece: ";
      cin >> ch;
      if (cin.fail()){
        cout << "\nWhat you entered was not an option, please try again" << endl;
        cin.clear();
        cin.ignore(100, '\n');
      }
      else if (find(redGrave.begin(), redGrave.end(), ch) != redGrave.end()){
        piece = ch;
        redGrave.erase(remove(redGrave.begin(), redGrave.end(), ch), redGrave.end());
        check3 = 1;
      }
      else{
        cout << "\nWhat you entered was not an option, please try again" << endl;
        cin.clear();
        cin.ignore(100, '\n');
      }
    } while (check3 == 0);


  }

  else if ((piece == 'P') && (row2 == 7)){
    int len = whiteGrave.size();
    char ch;
    int check3 = 0;
    do {
      printBoard(player);
      cout << "Please choose a fallen piece to replace your pawn with:\n\n  ";
      for (int i = 0; i<len; i++){
        if (whiteGrave[i] != 'P')
          cout << "'" << whiteGrave[i] << "'  ";
      }
      cout <<"\n\nWhich Piece: ";
      cin >> ch;
      if (cin.fail()){
        cout << "\nWhat you entered was not an option, please try again" << endl;
        cin.clear();
        cin.ignore(100, '\n');
      }
      else if (find(whiteGrave.begin(), whiteGrave.end(), ch) != whiteGrave.end()){
        piece = ch;
        whiteGrave.erase(remove(whiteGrave.begin(), whiteGrave.end(), ch), whiteGrave.end());
        check3 = 1;
      }
      else{
        cout << "\nWhat you entered was not an option, please try again" << endl;
        cin.clear();
        cin.ignore(100, '\n');
      }
    } while (check3 == 0);
  }

  //Finally replace the end location with desired piece
  board[row2][col2] = piece;
}

/*
bool Chess::kingValids(int row, int col, char player){
  //in this function we will take a users row and column and check the piece at that location
  //we will check that piece and location for all its possible moves and return all possibilities
  //in a 2-dimensional array

  char piece = board[row][col];

  //Prepare our return array
  int** moves = 0;
  moves = new int*[21];
  for (int i = 0; i < 21; i++){
    moves[i] = new int[2];
  }
  int cntr = 0;

  //Depending on the current player, set which array of characters is the opponent
  char redPlayer[9] = {'p','r','h','b','q','k', 'o','t','j'};
  char whitePlayer[9] = {'P', 'R', 'H', 'B', 'Q', 'K','O','T','J'};
  char opponent[9];
  char enemyKing1, enemyKing2;
  if (player == 'w'){
    enemyKing1 = 'k';
    enemyKing2 = 'j';
    memcpy(opponent, redPlayer, sizeof(opponent));
  }
  else if (player == 'r'){
    enemyKing1 = 'K';
    enemyKing2 = 'J';
    memcpy(opponent, whitePlayer, sizeof(opponent));
  }


  if ((piece == 'P')|| (piece == 'O')){
    //if piece is white pawn
    if (piece == 'O'){
      //if piece hasn't moved yet
      if ((row+2 >= 0) && (row+2 <= 7) && (col >= 0) && (col <= 7)){
        //if space directly in front is valid
        if (board[row +2][col] == '.'){
          moves[cntr][0] = row + 2;
          moves[cntr][1] = col;
          cntr += 1;
        }
      }
    }
    if ((row+1 >= 0) && (row+1 <= 7) && (col >= 0) && (col <= 7)){
      //if space directly in front is valid
      if (board[row +1][col] == '.'){
        moves[cntr][0] = row + 1;
        moves[cntr][1] = col;
        cntr += 1;
      }
    }
    if ((row+1 >= 0) && (row+1 <= 7) && (col-1 >= 0) && (col-1 <= 7)){

      char temp = board[row+1][col-1];
      if (find(begin(opponent), end(opponent), temp) != end(opponent)){
        moves[cntr][0] = row + 1;
        moves[cntr][1] = col - 1;
        cntr += 1;
      }
    }
    if ((row+1 >= 0) && (row+1 <= 7) && (col+1 >= 0) && (col+1 <= 7)){
      char temp2 = board[row+1][col+1];
      if (find(begin(opponent), end(opponent), temp2) != end(opponent)){
        moves[cntr][0] = row + 1;
        moves[cntr][1] = col + 1;
        cntr += 1;
      }
    }
  }

  else if ((piece == 'p') || (piece == 'o')){
    //if piece is black pawn
    if (piece == 'o'){
      //if piece hasn't moved yet
      if ((row-2 >= 0) && (row-2 <= 7) && (col >= 0) && (col <= 7)){
        //if space directly in front is valid
        if (board[row -2][col] == '.'){
          moves[cntr][0] = row - 2;
          moves[cntr][1] = col;
          cntr += 1;
        }
      }
    }
    if ((row-1 >= 0) && (row-1 <= 7) && (col >= 0) && (col <= 7)){
      //if space directly in front is valid
      if (board[row -1][col] == '.'){
        moves[cntr][0] = row - 1;
        moves[cntr][1] = col;
        cntr += 1;
      }
    }
    if ((row-1 >= 0) && (row-1 <= 7) && (col-1 >= 0) && (col-1 <= 7)){

      char temp = board[row-1][col-1];
      if (find(begin(opponent), end(opponent), temp) != end(opponent)){
        moves[cntr][0] = row - 1;
        moves[cntr][1] = col - 1;
        cntr += 1;
      }
    }
    if ((row-1 >= 0) && (row-1 <= 7) && (col+1 >= 0) && (col+1 <= 7)){
      char temp2 = board[row-1][col+1];
      if (find(begin(opponent), end(opponent), temp2) != end(opponent)){
        moves[cntr][0] = row - 1;
        moves[cntr][1] = col + 1;
        cntr += 1;
      }
    }
  }

  else if ((piece == 'H') || (piece == 'h')){
    int directions[8][2] = {{-2,-1},{-2,1},{-1,2},{1,2},{2,-1},{2,1},{-1,-2},{1,-2}};
    int val1, val2;
    char temp;
    for (int i = 0; i<8; i++){
      val1 = row+directions[i][0];
      val2 = col+directions[i][1];
      if ((val1 >= 0) && (val1 <= 7) && (val2 >= 0) && (val2 <= 7)){
        temp = board[val1][val2];
        if ((find(begin(opponent), end(opponent), temp) != end(opponent)) || (temp == '.')){
          moves[cntr][0] = val1;
          moves[cntr][1] = val2;
          cntr += 1;
        }
      }
    }
  }

  else if ((piece =='R') || (piece == 'r') || (piece == 'T') || (piece == 't')){
    //The following if/else statements help create the "castle" possibility
    if ((piece == 'T') && (row == 0) && (col == 0) && (board[row][1] == '.') && (board[row][2] == '.') && (board[row][3] == '.') && (board[0][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    else if ((piece == 'T') && (row == 0) && (col == 7) && (board[row][6] == '.') && (board[row][5] == '.') && (board[0][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    else if ((piece == 't') && (row == 7) && (col == 0) && (board[row][1] == '.') && (board[row][2] == '.') && (board[row][3] == '.') && (board[row][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    else if ((piece == 't') && (row == 7) && (col == 7) && (board[row][6] == '.') && (board[row][5] == '.')  && (board[row][4] == 'J')){
      moves[cntr][0]=0;
      moves[cntr][1]=3;
      cntr += 1;
    }
    int directions[4][2] = {{-1,0},{1,0},{0,-1},{0,1}};
    int tempVal1, tempVal2;
    char temp;
    for (int i=0; i<4; i++){
      tempVal1 = row;
      tempVal2 = col;
      while(1){
        tempVal1 +=directions[i][0];
        tempVal2 +=directions[i][1];
        if ((tempVal1 >= 0) && (tempVal1 <= 7) && (tempVal2 >= 0) && (tempVal2 <= 7)){
          temp = board[tempVal1][tempVal2];
          if (temp == '.'){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
          }
          else if ((find(begin(opponent), end(opponent), temp) != end(opponent))){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
            break;
          }
          else {
            break;
          }
        }
        else{
          break;
        }
      }
    }
  }

  else if ((piece == 'B') || (piece == 'b')){
    int directions[4][2] = {{-1,-1},{-1,1},{1,1},{1,-1}};
    int tempVal1, tempVal2;
    char temp;
    for (int i=0;i<4;i++){
      tempVal1=row;
      tempVal2=col;
      while(1){
        tempVal1 +=directions[i][0];
        tempVal2 +=directions[i][1];
        if ((tempVal1 >= 0) && (tempVal1 <= 7) && (tempVal2 >= 0) && (tempVal2 <= 7)){
          temp = board[tempVal1][tempVal2];
          if (temp == '.'){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
          }
          else if ((find(begin(opponent), end(opponent), temp) != end(opponent))){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
            break;
          }
          else {
            break;
          }
        }
        else{
          break;
        }
      }
    }
  }

  else if ((piece == 'Q') || (piece == 'q')){
    int directions[8][2] = {{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1}};
    int tempVal1, tempVal2;
    char temp;
    for (int i=0;i<8;i++){
      tempVal1=row;
      tempVal2=col;
      while(1){
        tempVal1 +=directions[i][0];
        tempVal2 +=directions[i][1];
        if ((tempVal1 >= 0) && (tempVal1 <= 7) && (tempVal2 >= 0) && (tempVal2 <= 7)){
          temp = board[tempVal1][tempVal2];
          if (temp == '.'){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
          }
          else if ((find(begin(opponent), end(opponent), temp) != end(opponent))){
            moves[cntr][0] = tempVal1;
            moves[cntr][1] = tempVal2;
            cntr += 1;
            break;
          }
          else {
            break;
          }
        }
        else{
          break;
        }
      }
    }
  }

  else if ((piece == 'K') || (piece == 'k') || (piece == 'J') || (piece == 'j')){
    int directions[8][2] = {{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1}};
    int val1, val2;
    char temp;
    for (int i = 0; i<8;i++){
      val1 = row+directions[i][0];
      val2 = col+directions[i][1];
      if ((val1 >= 0) && (val1 <= 7) && (val2 >= 0) && (val2 <= 7)){
        temp = board[val1][val2];
        if ((find(begin(opponent), end(opponent), temp) != end(opponent)) || (temp == '.')){
          moves[cntr][0] = val1;
          moves[cntr][1] = val2;
          cntr += 1;
        }
      }
    }
  }

//  cout << "\n\n\n";
//  for (int i = 0; i < 21; i++){
//    cout << "row: " << moves[i][0]+1 << "  col:  " << moves[i][1]+1 << '\n';
//  }
//  cout << "\n\n\n";


  int rw, cl;
  for (int i = 0; i<21; i++){
    rw = moves[i][0];
    cl = moves[i][0];
    if ((cl >= 0) && (cl <= 7) && (rw >= 0) && (rw <= 7)){
      if((board[rw][cl] == enemyKing1) || (board[rw][cl] == enemyKing2))
        return true;
    }
  }
  return false;
}

bool Chess::check(char player){
  //This function iterates through the board and calls the function to check if kings in checl

  for (int i = 0; i<8; i++){
    for (int j = 0; j<8; j++){
      if (kingValids(i,j,player) == 1){
          return true;
      }
    }
  }
  return false;
}
*/

bool Chess::checkKing(char player){
  //This function iterates through the board and checks if the opposite player still
  //has their king or not
  char king1, king2;
  if (player == 'w'){
    king1 = 'k';
    king2 = 'j';
  }
  else if (player == 'r'){
    king1 = 'K';
    king2 = 'J';
  }

  for (int i = 0; i<8; i++){
    for (int j = 0; j<8; j++){
      if ((board[i][j] == king1) || (board[i][j] == king2)){
          return true;
      }
    }
  }
  return false;
}

void Chess::runGame(){
  /*
The meat of the UX of the chess game.  It is the only method needed to be called
on a chess class to play it.  Sets the board and then the wile loop prompts user
input and correctly effects the board until the end game.

NOtable functionalitys

Checking if row==101.  If so, we have already moved
pieces around and can therefore skip calling make moves.
  */
  setBoard();
  char player = 'r';
  int *p;
  int **printable;
  int *m;
  bool fact;
  int checkk = 0;
  while(1){
    if (player == 'r')
      player = 'w';
    else if (player == 'w')
      player = 'r';
    printBoard(player);
    /*
    if (checkk == 1){
      cout << "\n\nWARNING!!! YOU ARE IN CHECK!!!\n\n";
      checkk = 0;
      sleep(4);
    }
    */
    redo:
    p = getPos(player);
    //cout << '\n' << board[p[0]][p[1]] << "\n\n";
    if (p[0] == 101){
    }
    else if (p[0] == -1){
      return;
    }
    else{
      printable = getValids(p[0],p[1],player);
      m = getMove(player);
      fact = isValid(printable, m);
      if (fact == 1){
        makeMove(p[0], p[1], m[0], m[1], 'w');
      }
      else {
        printBoard(player);
        cout << "\nThat was invalid move.  Try again\n\n";
        goto redo;
      }
    }
    /*
    if (check(player) == 1){
      checkk = 1;
    }
    */
    if (checkKing(player) == 0){
      break;
    }
  }
  if (player == 'r'){
    printBoard('l');
    cout << "\nCONGRATULATIONS RED PLAYER\n\nYOU HAVE WON\n\n";
    sleep(5);
    return;
  }
  else if (player == 'w'){
    printBoard('l');
    cout << "\nCONGRATULATIONS WHITE PLAYER\n\nYOU HAVE WON\n\n";
    sleep(5);
    return;
  }
}
