#ifndef CHESS_H_
#define CHESS_H_

#include <vector>
using namespace std;

class Chess
{
/*The following class is an easy to use console based chess simulator.  The team
colors are white and red and white always goes first.  Please play with a human partner.
Functionality includes castling and pawn replacement.  Please be sure to notify partner
when they are put in check

Dev stuff:
You will notice the board has 9 types of pieces.  That is because pawns, kings, and rooks
are represented by 2 states.  One having never been moved and one having been moved at
least once.  This is because pawns can move 2 spaces on ther first move and for castling*/
public:
  vector< char > whiteGrave;
  vector< char > redGrave;
  char **board = nullptr;

  int * getPos(char player);

  int * getMove(char player);

  void makeMove(int row1, int col1, int row2, int col2, char player);

  void printBoard(char player);

  void setBoard();

  int** getValids(int row, int col, char player);

  bool isValid(int **moves, int *move);

  bool checkKing(char player);
/*
  bool check(char player);

  bool kingValids(int row, int col, char player);
*/
  void runGame();
};

#endif
